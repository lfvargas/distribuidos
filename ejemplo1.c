#include <stdio.h>
#include <string.h>
#include <mpi.h>

#define  MASTER     0

int main(int argc, char **argv) {
    int my_rank, numtasks, tag = 0 , len;
    int provided, claimed;
    char msg[20];
    MPI_Status status;
    char hostname[MPI_MAX_PROCESSOR_NAME];

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
    if (my_rank == 0) {
        strcpy(msg, "Hello ");
        MPI_Send(msg, strlen(msg) + 1, MPI_CHAR, 1, tag, MPI_COMM_WORLD);
    } else if (my_rank == 1) {
        MPI_Recv(msg, 20, MPI_CHAR, 0, tag, MPI_COMM_WORLD, &status);
    }


    MPI_Query_thread( &claimed );
        printf( "Query thread level= %d  ", claimed );
        //printf( "Query thread level= %d  Init_thread level= %d\n", claimed, provided );
   // this one is obvious  
   MPI_Get_processor_name(hostname, &len);

   if(my_rank % 2){
    printf ("Number of tasks= %d My rank= %d Running on %s\n", numtasks,my_rank,hostname);
   }else {
    printf ("Number of tasks= %d My rank= %d \n", numtasks,my_rank);
   }

   if (my_rank == MASTER)
    printf("Yo soy Maestro!! numero de tareas: %d\n",numtasks);

    MPI_Finalize();
}
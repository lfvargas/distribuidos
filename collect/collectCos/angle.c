#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int main(int argc, char** argv) {
        double angle;
        char *filename;
        FILE *fd;
        srand(time(NULL));
	angle = rand() % 360;
        if (argc != 2) {
                printf("This program requires one arguments <filename>\n");
                exit(-1);
        }
        filename = (char*)malloc(sizeof(char)*strlen(argv[1]));
        strcpy(filename,argv[1]);
        fd = fopen(filename,"w");
        if (fd == NULL) {
                printf("File can not be created\n");
                exit(-1);
        }
        fprintf(fd,"%lf",angle);
        fclose(fd);
}


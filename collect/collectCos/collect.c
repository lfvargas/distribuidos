#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char** argv) {
        double x,y;
        char *filename1;
	char *filename2;
        FILE *fd;
        if (argc != 3) {
                printf("This program requires one arguments <filename> <filename>\n");
                exit(-1);
        }
        filename1 = (char*)malloc(sizeof(char)*strlen(argv[1]));
	filename2 = (char*)malloc(sizeof(char)*strlen(argv[2]));
        strcpy(filename1,argv[1]);
	strcpy(filename2,argv[2]);
        fd = fopen(filename1,"r");

        if (fd == NULL) {
                printf("File 1 can not be created\n");
                exit(-1);
        }
        fscanf(fd,"%lf",&x);

	fd = fopen(filename2,"r");
	if(fd==NULL){
		printf("File 2 can not be created\n");
        	exit(-1);
	}
	fscanf(fd,"%lf",&y);
	fclose(fd);
	printf("Result:%f\n", x+y);
}


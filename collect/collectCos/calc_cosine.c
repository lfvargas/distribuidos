#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define PI 3.14159265

int main(int argc, char** argv) {
        double x,y;
        char *filename1;
	char *filename2;
        FILE *fd;
        if (argc != 3) {
                printf("This program requires one arguments <filename_in> <filename_out>\n");
                exit(-1);
        }
	filename1 = (char*)malloc(sizeof(char)*strlen(argv[1]));
	strcpy(filename1,argv[1]);
	fd = fopen(filename1,"r");
	if(fd==NULL){
		printf("File can not be read\n");
		exit(-1);
	}
	fscanf(fd,"%lf",&x);
	y = pow((cos(x*(PI/180))), 2.0);
        filename2 = (char*)malloc(sizeof(char)*strlen(argv[2]));
        strcpy(filename2,argv[2]);
        fd = fopen(filename2,"w");

        if (fd == NULL) {
                printf("File can not be created\n");
                exit(-1);
        }
        fprintf(fd,"%lf",y);
        fclose(fd);
}


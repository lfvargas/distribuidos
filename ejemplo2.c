#include <stdio.h>
#include <string.h>
#include <mpi.h>
#include <ctype.h>
#define  MASTER     0

int main(int argc, char **argv) {
	int my_rank, size, tag = 0, dest, source;
	char outmsg[20],inmsg[20];
	MPI_Status status;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);


	if (my_rank == MASTER) {
		dest = 1;
		source = 1;
		strcpy(outmsg, "Mensaje Master");
	 	MPI_Sendrecv (&outmsg, 20,  MPI_CHAR, dest, tag,&inmsg, 20, MPI_CHAR, source, tag, MPI_COMM_WORLD, &status);
	 	
	 	/*el overload puede ocurrir en esta zona de código, si se altera el orden de las mismas
		porque el metodo Recv va a generar un bloqueo mientras espera un Send de un proceso que está
		esperando recibir algo del maestro  
		*/
		//MPI_Send(msg, strlen(msg) + 1, MPI_CHAR, 1, tag, MPI_COMM_WORLD);
		// MPI_Recv(msg,20,MPI_CHAR,1,tag,MPI_COMM_WORLD,&status);
		//printf("%s (Mensaje recibido en Master) \n", inmsg);
		
		
	} else if (my_rank == 1) {
		dest = 0;
		source = 0;
		strcpy(outmsg, "Mensaje Esclavo1");
		MPI_Sendrecv (&outmsg, 20,  MPI_CHAR, dest, tag,&inmsg, 20, MPI_CHAR, source, tag, MPI_COMM_WORLD, &status);
		
		//MPI_Recv(msg, 20, MPI_CHAR, 0, tag, MPI_COMM_WORLD, &status);
		//printf("%s (Mensaje recibo en Esclavo1) \n", msg);
		 /*
		 int i=0;
		 while(msg[i])
		   {
		      putchar (toupper(msg[i]));
		      i++;
		   }
		 */
		 //MPI_Send(msg, strlen(msg)+1,MPI_CHAR,0,tag, MPI_COMM_WORLD);

		//
		
	}
	MPI_Finalize();
}
